import pathlib
import random

with open(
    pathlib.Path(__file__).parent / "namelists" / "nouns.txt", "r", encoding="utf-8"
) as f:
    nouns = [line.strip() for line in f.readlines()]

with open(
    pathlib.Path(__file__).parent / "namelists" / "adjectives.txt",
    "r",
    encoding="utf-8",
) as f:
    adjectives = [line.strip() for line in f.readlines()]


def get_random_name():
    return f"{random.choice(adjectives)}{random.choice(nouns)}".capitalize()
