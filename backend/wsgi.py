from flask import Flask, session
from flask_restful import Api, Resource

from common.character import (
    Character,
    IllegalEatingAlreadyEaten,
    IllegalEatingEmpty,
    IllegalMoveBlocked,
    IllegalMoveBorder,
)
from common.grid import Grid
from common.redis_connection import get_redis

meadow_height = 32
meadow_width = 32


app = Flask(__name__)
api = Api(app)

# ToDo fix this
app.config["SECRET_KEY"] = "secret"


class CharacterResource(Resource):
    def get_character(self) -> Character:
        redis = get_redis()
        grid = Grid(redis)

        character_id = session.get("character_id", None)
        if character_id:
            character = Character.from_redis(redis, grid, character_id)
        else:
            character = None

        if not character:
            character = Character.new_from_redis(redis, grid)
            session["character_id"] = character.character_id

        return character


class MyCharacter(CharacterResource):
    def get(self):
        return self.get_response()

    def get_response(self, character=None):
        if character is None:
            character = self.get_character()
        return {
            "name": character.name,
            "facing": character.direction_readable,
            "hungry": character.fullness == 0,
            "fullness": character.fullness,
        }


class Look(CharacterResource):
    def get(self):
        return self.get_response()

    def get_response(self, character=None):
        if character is None:
            character = self.get_character()
        observed = character.look_forward()
        seeing = {"what": Character.OBSERVABLES[observed.entity_type]}
        if observed.entity_name:
            seeing["name"] = observed.entity_name

        if observed.extra:
            seeing["extra"] = observed.extra

        result = {
            "facing": character.direction_readable,
            "seeing": seeing,
        }
        return result


class Move(Look):
    def post(self):
        character = self.get_character()
        extra_result = {}
        try:
            character.move_forward()
            extra_result["moved"] = True
        except IllegalMoveBlocked:
            extra_result["moved"] = False
            extra_result["thinking"] = "There is something there"
        except IllegalMoveBorder:
            extra_result["moved"] = False
            extra_result["thinking"] = "I don't want to go there"

        result = self.get_response(character=character)
        result.update(extra_result)

        return result


class Eat(Look):
    def post(self):
        character = self.get_character()
        extra_result = {}
        try:
            character.eat()
            extra_result["eaten"] = True
        except IllegalEatingEmpty:
            extra_result["eaten"] = False
            extra_result["thinking"] = "There is nothing to eat in front of me"
        except IllegalEatingAlreadyEaten:
            extra_result["eaten"] = False
            extra_result["thinking"] = "Someone just ate it from under my nose"

        result = self.get_response(character=character)
        result.update(extra_result)

        return result


class Turn(Look):
    def post(self, direction):
        if direction in ["left", "counterclockwise"]:
            turn = -1
        elif direction in ["right", "clockwise"]:
            turn = 1
        elif direction == "around":
            turn = 2
        else:
            result = self.get()
            result["turned"] = False
            result["thinking"] = f"Don't know how to turn {direction}"
            return result

        character = self.get_character()
        character.turn(turn)
        result = self.get_response(character=character)
        result["turned"] = True

        return result


class Leaderboard(Resource):
    def get(self):
        redis = get_redis()
        grid = Grid(redis)
        grid.update_all_from_redis()

        horses = [item for item in grid.objects.values() if isinstance(item, Character)]
        horses.sort(key=lambda item: -item.fullness)

        return [{"name": item.name, "fullness": item.fullness} for item in horses]


api.add_resource(MyCharacter, "/")
api.add_resource(Move, "/move/")
api.add_resource(Look, "/look/")
api.add_resource(Eat, "/eat/")
api.add_resource(Turn, "/turn/<string:direction>/")
api.add_resource(Leaderboard, "/leaderboard/")


if __name__ == "__main__":
    app.run(debug=True)
